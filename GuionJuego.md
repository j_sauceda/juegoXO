# Juego X-0

Temas:

- Tailwind CSS
- Deploy (Cargar a un servidor) to Vercel
- PWA (Progressive Web App)

Referencias:

- [Tutorial juego X-O](https://reactjs.org/tutorial/tutorial.html)
- [Video PWA en español](https://www.youtube.com/watch?v=p9uyTrvgNEE)
- [PWA + Deploy](https://www.youtube.com/watch?v=G0bBLvWXBvc)
- [Tailwind + Angular](https://youtu.be/IYI0em-xT28?t=10284)

## Pasos iniciales

- $ ng new juego // no routing, con CSS
- $ cd juego

## Tailwind

- $ npm install -D tailwindcss postcss autoprefixer
- $ npx tailwindcss init
- editar tailwind.config.js:

```JS
/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ["./src/**/*.{html,ts}",],
  theme: {
    extend: {},
  },
  plugins: [],
}
```

- editar styles.css:

```CSS
@tailwind base;
@tailwind components;
@tailwind utilities;
```

- opcional: en vs-code instalar la extensión "Tailwind CSS Intellisense"
- limpiar app.component.html:

```xml
<div class="flex flex-col h-screen dark:bg-slate-600">
  <nav class="flex justify-between p-4 bg-red-600 dark:bg-slate-800 text-white">
    <div>X-O</div>
    <button
      class="h-10 px-6 font-semibold rounded-md bg-slate-700 text-white dark:bg-white dark:text-black"
      (click)="toggle_theme()"
    >
      Color theme
    </button>
  </nav>

  <div class="md:container md:mx-auto font-sans flex-grow dark:text-white">
    <h1 class="text-3xl font-bold underline">Aqui va el juego</h1>
  </div>

  <footer class="bg-gray-200 dark:bg-gray-800 text-center lg:text-left">
    <div
      class="text-gray-700 dark:text-white text-center p-4"
      style="background-color: rgba(0, 0, 0, 0.2)"
    >
      Built with Angular - © 2022 Copyrigh:
      <a
        class="text-gray-800 dark:text-white"
        href="https://gitlab.com/j_sauceda"
        >Jorge Sauceda</a
      >
    </div>
  </footer>
</div>
```

- preparar app.component.ts:

```JS
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'juego';
  dark_theme = false;

  toggle_theme() {
    this.dark_theme = !this.dark_theme;
    if (!this.dark_theme) {
      document.documentElement.classList.add('dark');
    } else {
      document.documentElement.classList.remove('dark');
    }
  }
}
```

- $ ng serve

## Componentes

- $ ng g component squares --inline-style --inline-template
- $ ng g component board
- editar squares.component.ts:

```javascript
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-squares',
  template: `
    <div
      class="dark:bg-gray-500 object-fill font-semibold rounded-md text-center text-[130px]"
      [ngClass]="value === '' ? 'bg-slate-300' : ''"
      [ngClass]="
        value === 'X'
          ? 'bg-blue-300 dark:bg-orange-700'
          : 'bg-green-300 dark:bg-teal-500'
      "
    >
      {{ value }}
    </div>
  `,
  styles: [],
})
export class SquaresComponent {
  @Input() value = '';
}
```

- editar board.component.ts:

```JS
import { Component } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css'],
})
export class BoardComponent {
  squares!: string[];
  xIsNext!: boolean;
  winner!: string | null;

  constructor() {}

  ngOnInit() {
    this.newGame();
  }

  newGame() {
    this.squares = Array(9).fill('');
    this.winner = null;
    this.xIsNext = true;
  }

  get player() {
    return this.xIsNext ? 'X' : 'O';
  }

  makeMove(idx: number) {
    if (!this.squares[idx]) {
      this.squares.splice(idx, 1, this.player);
      this.xIsNext = !this.xIsNext;
    }

    this.winner = this.calculateWinner();
  }

  calculateWinner() {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        this.squares[a] &&
        this.squares[a] === this.squares[b] &&
        this.squares[a] === this.squares[c]
      ) {
        return this.squares[a];
      }
    }
    return null;
  }
}
```

- editar board.component.html:

```xml
<h1 class="mb-1">Es el turno del jugador: {{ player }}</h1>
<button
  nbButton
  outline
  class="h-10 px-6 mb-2 font-semibold rounded-md bg-red-400"
  (click)="newGame()"
>
  Nuevo Juego
</button>

<h2 class="mb-2" *ngIf="winner">Las {{ winner }} han ganado!</h2>

<div class="grid grid-rows-3 grid-cols-3 gap-1">
  <app-squares
    *ngFor="let val of squares; let i = index"
    [value]="val"
    (click)="makeMove(i)"
  >
  </app-squares>
</div>
```

- editar board.component.css:

```css
app-squares {
    border: 1px gray solid;
    height: 200px
}
```


- editar app.component.html:

```xml
<div class="flex flex-col h-screen dark:bg-slate-600">
  <nav class="flex justify-between p-4 bg-red-600 dark:bg-slate-800 text-white">
    <div>X-O</div>
    <button
      class="h-10 px-6 font-semibold rounded-md bg-slate-700 text-white dark:bg-white dark:text-black"
      (click)="toggle_theme()"
    >
      Color theme
    </button>
  </nav>

  <div class="md:container md:mx-auto font-sans flex-grow dark:text-white">
    <app-board></app-board>
  </div>

  <footer class="bg-gray-200 dark:bg-gray-800 text-center lg:text-left">
    <div
      class="text-gray-700 dark:text-white text-center p-4"
      style="background-color: rgba(0, 0, 0, 0.2)"
    >
      Built with Angular - © 2022 Copyrigh:
      <a
        class="text-gray-800 dark:text-white"
        href="https://gitlab.com/j_sauceda"
        >Jorge Sauceda</a
      >
    </div>
  </footer>
</div>
```

## PWA

- lea esos enlaces para obtener mayor información sobre [la especificación PWA](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps), [las PWA en el navegador](https://opensource.com/article/22/10/pwa-web-browser), y [el uso de PWA como service worker](https://docs.angular.lat/guide/service-worker-intro)
- $ ng add @angular/pwa

  - @angular/pwa agrega "@angular/service-worker" a las dependencias de package.json
  - Habilita la compilación de service workers desde la CLI
  - Importa ServiceWorkerModule y lo carga en imports en app.module.ts
  - en el <head> de index.html agrega <link rel="manifest" ...> y <meta name="theme-color"...>
  - instala íconos en /src/assets/icons/ para soportar PWA
  - crea el archivo de configuración ngsw-config.json

- editar manifest.webmanifest:

```json
{
  "name": "Mi juego de X-O Angular",
  "short_name": "X-O Angular",
}
```

- $ ng build --configuration "production" --aot // aot: Ahead of time compilation
- $ npm install lite-server --save-dev
- $ npx lite-server --baseDir="dist/juego"

## Deploy

- lea los siguientes enlaces para obtener mayor información sobre [soporte de angular en vercel](https://vercel.com/solutions/angular), [auto detección de angular](https://vercel.com/guides/deploying-angular-with-vercel), [configuración via vercel.json](https://vercel.com/docs/project-configuration), y [variables de entorno en vercel](https://vercel.com/docs/concepts/projects/environment-variables)
- $ npm install vercel
- NOTA: en los siguientes pasos optamos por desplegar nuestro projecto sin editar vercel.json
- $ cd /dist/juego/
- $ vercel
  - set and deploy: yes
  - link to existing proyect: no
  - what's your project name? angular-xo-pwa
  - in which directory is your code loaded? ./
  - local settings detected in vercel.json:
  	- build command: `npm run vercel-build` or `npm install`
  	- development command: `yarn install`, `pnpm install` or `npm install`
  	- output directory: `public` if it exists, or `.`
  	- ? Want to modify these settings? no
  - linked to url
  - inspect: url
  - production: https://angular-xo-pwa.vercel.app/
  - deployed to production: url
  - to change the domain or build command, go to url

## Git

- lea los siguientes enlaces para [una introducción a los conceptos Git](https://opensource.com/article/22/11/git-concepts), y el [uso básico de gitlab](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
- $ git init --initial-branch=master
- $ git remote add origin https://gitlab.com/j_sauceda/juegoXO.git
- $ git add .
- $ git commit -m "Initial commit"
- $ git push -u origin master
