import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-squares',
  template: `
    <div
      class="dark:bg-gray-500 object-fill font-semibold rounded-md text-center text-[130px]"
      [ngClass]="value === '' ? 'bg-slate-300' : ''"
      [ngClass]="
        value === 'X'
          ? 'bg-blue-300 dark:bg-orange-700'
          : 'bg-green-300 dark:bg-teal-500'
      "
    >
      {{ value }}
    </div>
  `,
  styles: [],
})
export class SquaresComponent {
  @Input() value = '';
}
