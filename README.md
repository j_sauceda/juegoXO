# Juego

The purpose of this small project is to demonstrate the use of @angular/pwa and Tailwind CSS to create a responsive Progressive Web Application with Angular 15. It was coded as part of an introductory course on Angular taught in Spanish for [elev8](https://www.elev8me.com).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.0.

## Cloning this repo

Run `npm install` to clone this repository in your local machine

## Deployment demo

For demonstration purposes, this project has been deployed to Vercel and can be accesed [here](https://angular-xo-pwa.vercel.app/)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
